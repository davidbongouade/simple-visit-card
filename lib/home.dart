import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Developpeur Flutter", style: TextStyle(fontSize: 32.0)),
                  SizedBox(
                    height: 5.0,
                  ),
                  decor(),
                  spaceC(),
                  CircleAvatar(
                    radius: 100.0,
                    child: CircleAvatar(
                      radius: 97.0,
                      backgroundImage: AssetImage('assets/David.jpg'),
                    ),
                  ),
                  spaceC(),
                  Text(
                    "David BONGOUADE",
                    style: TextStyle(fontSize: 24),
                  ),
                  spaceC(),
                  Padding(
                    padding: const EdgeInsets.only(
                      // top: 5.0,
                      left: 15.0,
                    ),
                    child: Column(
                      children: [
                        Row(children: [
                          FaIcon(
                            FontAwesomeIcons.whatsapp,
                            size: 30.0,
                            color: Colors.black,
                          ),
                          spaceR(),
                          InkWell(
                            onTap: () => _launchURL(
                              url: "http:wa.me/+22566066299",
                            ),
                            child: Text(
                              "Whatsapp",
                              style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ]),
                        spaceL(),
                        line(
                          icon: Icons.phone,
                          text: "Call",
                          link: "tel:+22566066299",
                        ),
                        spaceL(),
                        Row(
                          children: [
                            FaIcon(
                              FontAwesomeIcons.gitlab,
                              size: 30.0,
                              color: Colors.black,
                            ),
                            spaceR(),
                            InkWell(
                              onTap: () =>
                                  _launchURL(url: "http:gitlab.com/bongouade"),
                              child: Text(
                                "GitLab",
                                style: TextStyle(fontSize: 18),
                              ),
                            )
                          ],
                        ),
                        spaceL(),
                        Row(
                          children: [
                            FaIcon(
                              FontAwesomeIcons.github,
                              size: 30.0,
                              color: Colors.black,
                            ),
                            spaceR(),
                            InkWell(
                              onTap: () => _launchURL(
                                url: "http:github.com/bongouade",
                              ),
                              child: Text(
                                "GitHub",
                                style: TextStyle(fontSize: 18),
                              ),
                            )
                          ],
                        ),
                        spaceL(),
                        line(
                          icon: Icons.mail,
                          text: "E-mail",
                          link: "mailto:bongouade@gmail.com",
                        ),
                        spaceL(),
                        line2(
                          icon: Icons.location_on,
                          text: "Abidjan, Côte d'Ivoire",
                        ),
                        spaceL(),
                        line2(
                          icon: Icons.business,
                          text: "ABRAHAM",
                        ),
                        spaceL(),
                        line2(
                          icon: Icons.supervised_user_circle,
                          text: "Fondateur",
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget line({
    @required IconData icon,
    @required String text,
    @required String link,
  }) {
    return Row(
      children: [
        Icon(
          icon,
          size: 30.0,
          color: Colors.black,
        ),
        spaceR(),
        InkWell(
          onTap: () => _launchURL(
            url: link,
          ),
          child: Text(
            text,
            style: TextStyle(
              fontSize: 18,
              color: Colors.black,
            ),
          ),
        ),
      ],
    );
  }

  Widget line2({
    @required IconData icon,
    @required String text,
  }) {
    return Row(
      children: [
        Icon(
          icon,
          size: 30.0,
          color: Colors.black,
        ),
        spaceR(),
        Text(
          text,
          style: TextStyle(
            fontSize: 18,
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget decor() {
    return Container(
      height: 1.0,
      width: 200.0,
      color: Colors.grey,
    );
  }

  Widget spaceC() {
    return SizedBox(
      height: 30.0,
    );
  }

  Widget spaceL() {
    return SizedBox(
      height: 10.0,
    );
  }

  Widget spaceR() {
    return SizedBox(
      width: 15.0,
    );
  }

  _launchURL({@required String url}) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Erreur';
    }
  }
}
